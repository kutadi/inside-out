﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level1_PortalTextureSetup : MonoBehaviour
{
    public Camera camera_at_Corridor;
    public Material cameraMat_at_Corridor;

    public Camera camera_at_pinkroom;
    public Material cameraMat_at_pinkroom;

    // Start is called before the first frame update
    void Start()
    {
        if (camera_at_Corridor.targetTexture != null) {
            camera_at_Corridor.targetTexture.Release();
        }
        camera_at_Corridor.targetTexture = new RenderTexture(Screen.width, Screen.height, 24);
        cameraMat_at_Corridor.mainTexture = camera_at_Corridor.targetTexture;

        if (camera_at_pinkroom.targetTexture != null)
        {
            camera_at_pinkroom.targetTexture.Release();
        }
        camera_at_pinkroom.targetTexture = new RenderTexture(Screen.width, Screen.height, 24);
        cameraMat_at_pinkroom.mainTexture = camera_at_pinkroom.targetTexture;
    }
}
