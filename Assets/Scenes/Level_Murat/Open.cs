﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Open : MonoBehaviour
{
    public GameObject keyExit;

    private int countEnter = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Chest");
        var animator = GetComponentInParent<Animator>();
        if (other.gameObject.name == "Player" && Inventory.hasKey)
        {
            animator.SetBool("Open", true);
            Debug.Log(Inventory.hasKey);
        }

        if (animator.GetBool("Open") && countEnter >= 3)
        {
            keyExit.gameObject.SetActive(false);
            Inventory.hasExitKey = true;
        }

        countEnter++;
    }

    void OnCollisionStay(Collision other)
    {
        
    }
}
