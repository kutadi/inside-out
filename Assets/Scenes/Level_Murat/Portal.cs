﻿using UnityEngine;

namespace Assets.Scenes.Level_Murat
{
    public class Portal : MonoBehaviour
    {
        public GameObject room2;
        public GameObject room3;
        public GameObject room4;
        public GameObject wall_exit;
        public GameObject door;
        private static int _currentRoom = 1;

        // Start is called before the first frame update
        void Start()
        {
           //room2.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {
        
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.name.Contains("Player") || other.gameObject.name.Contains("Camera") )
            {
                if (_currentRoom == 1 && gameObject.name.Equals("Portal1"))
                {
                   Show_room2();
                }

                if (_currentRoom == 2 )
                {
                    Debug.Log("room2");
                    if (gameObject.name.Equals("Portal2"))
                    {
                        ShowRoom3();
                    }
                }

                if (_currentRoom == 3)
                {
                    wall_exit.gameObject.SetActive(false);
                    door.SetActive(true);
                    
                    if (gameObject.name.Equals("Portal3"))
                    {
                        ShowRoom4();
                    }

                    if (gameObject.name.Equals("Portal2") && Inventory.hasKey)
                    {
                        room3.SetActive(false);
                        Show_room2();
                        wall_exit.gameObject.SetActive(true);
                        door.SetActive(false);
                    }

                    if (Inventory.hasExitKey)
                    {
                        wall_exit.gameObject.SetActive(false);
                    }
                }

                if (_currentRoom == 4)
                {
                    wall_exit.gameObject.SetActive(true);
                    door.SetActive(false);
                    if (gameObject.name.Equals("Portal4"))
                    {
                        room4.SetActive(false);
                        room3.SetActive(true);
                        wall_exit.gameObject.SetActive(false);
                        door.SetActive(true);
                        _currentRoom = 3;
                    }
                }
            }
        }

        private void Show_room2()
        {
            room2.SetActive(true);
            //camera2.gameObject.SetActive(true);
            _currentRoom = 2;
            Debug.Log("Room2");
        }

        private void ShowRoom3()
        {
            //camera2.gameObject.SetActive(false);
            //camera3.gameObject.SetActive(true);
            room2.SetActive(false);
            room3.SetActive(true);
            _currentRoom = 3;
            Debug.Log("Room3");
        }

        private void ShowRoom4()
        {
            _currentRoom = 4;
            room4.SetActive(true);
        }
    }
}
