﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera2 : MonoBehaviour
{
    public Transform playerCamera;
    private float xDiff, yDiff, zDiff;

    // Start is called before the first frame update
    void Start()
    {
        xDiff = playerCamera.position.x - transform.position.x;
        yDiff = playerCamera.position.y - transform.position.y;
        zDiff = playerCamera.position.z - transform.position.z;
        Debug.Log(xDiff);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(playerCamera.position.x - xDiff, playerCamera.position.y - yDiff, playerCamera.position.z - zDiff);
        transform.rotation = playerCamera.rotation;
    }
}
