﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerBig : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.transform.localScale += new Vector3(1, 1, 1);
        }
    }
}
