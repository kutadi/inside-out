﻿using UnityEngine;

public class Fade : MonoBehaviour {
    public Animator animator;
    private bool fadeOutFinish = false;
    private bool fadeInFinish = false;


    public void FadeOut()
    {
        animator.SetTrigger("FadeOut");
    }

    public void OnFadeOutComplete()
    {
        fadeOutFinish = true;
    }

    public void OnFadeInComplete()
    {
        fadeInFinish = true;
    }

    public bool IsFadeOutFinish()
    {
        return fadeOutFinish;
    }

    public bool IsFadeInFinish()
    {
        return fadeInFinish;
    }
}
