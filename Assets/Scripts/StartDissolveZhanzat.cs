﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartDissolveZhanzat : MonoBehaviour
{
    public Material initialMaterial;
    public Material dissolveMaterial;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Renderer>().material = initialMaterial;

    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Getable")
        {
            GetComponent<Renderer>().material = dissolveMaterial;
            Destroy(gameObject, 2.0f);
        }
    }
}
