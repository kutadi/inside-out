﻿using UnityEngine;
public class Level0Door : MonoBehaviour
{
    [Header("Door Object")]
    [SerializeField] private Animator myDoor = null;

    [Header("Trigger Type")]
    [SerializeField] private bool openTrigger = true;
    [SerializeField] private bool closeTrigger = false;

    [Header("Animation Name")]
    [SerializeField] private string doorOpen = "DoorOpen";
    [SerializeField] private string doorClose = "DoorClose";

    public Fade fade;
    public LevelManager levelManager;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Contains("Player") || other.tag.Equals("Player"))
        {
            if (openTrigger)
            {
                myDoor.Play(doorOpen, 0, 0.0f);
                gameObject.SetActive(false);
                if (fade != null)
                {
                    fade.FadeOut();
                    levelManager.SetCurrentLevel("StartScene");
                }
            }

            else if (closeTrigger)
            {
                myDoor.Play(doorClose, 0, 0.0f);
                gameObject.SetActive(false);
            }
        }
    }
}
