﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class credits : MonoBehaviour
{
    public Fade fade;
    public LevelManager levelManager;
    public GameObject creditsMenu;
    public GameObject player;
    public Animation anim;

    public void StartCredits()
    {
        player.GetComponent<FirstPersonController>().enabled = false;
    }
    
    public void StopCredits()
    {
        levelManager.SetCurrentLevel("StartScene");
        fade.FadeOut();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag.Equals("Player"))
        {
            creditsMenu.SetActive(true);
            anim.Play("credits");
        }
    }
}
