﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class EscapeMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;
        
    public GameObject pauseMenuUi;

    public Fade fade;
    public LevelManager levelManager;

    public FirstPersonController fpc;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        pauseMenuUi.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
        //fpc.GetComponent<FirstPersonController>().enabled = true;
        //fpc.paused = false;
        Cursor.visible = false;
    }

    void Pause()
    {
        pauseMenuUi.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
        //fpc.GetComponent<FirstPersonController>().enabled = false;
        //fpc.paused = true;
        Cursor.visible = true;
    }

    public void MainMenu()
    {
        Resume();
        fade.FadeOut();
        levelManager.SetCurrentLevel("MainMenu");
    }

    public void Restart()
    {
        Resume();
        fade.FadeOut();
        levelManager.SetCurrentLevel(SceneManager.GetActiveScene().name);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
