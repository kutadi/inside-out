﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Into : MonoBehaviour
{
    public Fade fade;

    void Update()
    {
        if(fade.IsFadeOutFinish())
        {
            SceneManager.LoadScene("StartScene");
        }    
    }
    public void Fade()
    {
        fade.FadeOut();
    }
}
