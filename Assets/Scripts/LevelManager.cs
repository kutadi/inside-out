﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public Fade fade;

    private string currentLevel = "Level1";

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(fade != null)
        {
            if(fade.IsFadeOutFinish())
            {
                SceneManager.LoadScene(currentLevel);
            }
        }
    }

    public string GetCurrentLevel()
    {
        return currentLevel;
    }

    public void SetCurrentLevel(string name)
    {
        currentLevel = name;
    }
}
