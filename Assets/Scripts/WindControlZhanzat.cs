﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class WindControlZhanzat : MonoBehaviour
{
    public float WindStrengthMin = 0;
    public float radius = 100;

    float windStrength;
    int i;
    RaycastHit hit;
    Collider[] hitColliders;
    Rigidbody rb;

    void Update()
    {
        float WindStrengthMax = gameObject.transform.localScale.y;
        windStrength = Random.Range(WindStrengthMin, WindStrengthMax);

        hitColliders = Physics.OverlapSphere(transform.position, radius);

        for (i = 0; i < hitColliders.Length; i++)
        {
            if (rb = hitColliders[i].GetComponent<Rigidbody>())
                if (Physics.Raycast(transform.position, rb.position - transform.position, out hit))
                    if (hit.transform.GetComponent<Rigidbody>() && WindStrengthMax > 1)
                        rb.AddForce(transform.forward * WindStrengthMax, ForceMode.Acceleration);
        }
    }
}
