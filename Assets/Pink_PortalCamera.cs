﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pink_PortalCamera : MonoBehaviour
{
    public Transform playerCamera;
    public Transform portal;
    public Transform otherPortal;

    // Update is called once per frame
    void Update()
    {
        Vector3 playerOffsetFromPortal = playerCamera.position - otherPortal.position;
        //transform.position = portal.GetChild(0).position;// + playerOffsetFromPortal;

        float angularDifferenceBetweenPortalRotations = Quaternion.Angle(portal.rotation, otherPortal.rotation);

        Quaternion portalRotationalDifference = Quaternion.AngleAxis(angularDifferenceBetweenPortalRotations, Vector3.up);
        //portalRotationalDifference *= Quaternion.Euler(0, -90, 0);
        Vector3 newCameraDirection = portalRotationalDifference * playerCamera.forward;
        //newCameraDirection = Quaternion.Euler(0, 90, 0);
        transform.rotation = Quaternion.LookRotation(newCameraDirection, Vector3.up);
    }
}
