﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalScript : MonoBehaviour
{

    public GameObject other;
    public Camera portalView;

    // Start is called before the first frame update
    void Start()
    {
        //other.portalView.targetTxture = new RenderTexture(Screen.width, Screen.height, 24);
        //GetComponent<MeshRenderer>().sharedMaterial.mainTexture = other.portalView.targetTexture;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 playerPosition = other.transform.worldToLocalMatrix.MultiplyPoint3x4(Camera.main.transform.position);
        portalView.transform.localPosition = -playerPosition;

        Quaternion difference = transform.rotation * Quaternion.Inverse(other.transform.rotation * Quaternion.Euler(0, 180,0));
        portalView.transform.rotation = difference * Camera.main.transform.rotation;
    }
}
