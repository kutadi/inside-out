﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class StepThrooghPortal : MonoBehaviour
{
    public Transform player;
    public GameObject otherPortal;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.tag + " hit the portal");
        if(other.tag == "Player")
        {
            CharacterController controller = GetComponent<CharacterController>();
            //player.transform.position = otherPortal.transform.position + otherPortal.transform.forward * 1;
            player.transform.position = otherPortal.transform.position; // + otherPortal.transform.forward * 1;
            FirstPersonController fpc = player.gameObject.GetComponent<FirstPersonController>();
            fpc.SetRotation(otherPortal.transform);
        }
    }
}
