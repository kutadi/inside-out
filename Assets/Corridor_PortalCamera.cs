﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Corridor_PortalCamera : MonoBehaviour
{
    public Transform playerCamera;
    public Transform portal;
    public Transform otherPortal;

    // Update is called once per frame
    void Update()
    {
        Vector3 playerOffsetFromPortal = otherPortal.position - playerCamera.position;
        Debug.Log(playerOffsetFromPortal);
        float dist = Vector3.Distance(otherPortal.position, playerCamera.position);
        //transform.position = portal.position + playerOffsetFromPortal;
        //transform.position = portal.position * dist;
        //transform.position *= dist;// + playerOffsetFromPortal;

        float angularDifferenceBetweenPortalRotations = Quaternion.Angle(portal.rotation, otherPortal.rotation);

        Quaternion portalRotationalDifference = Quaternion.AngleAxis(angularDifferenceBetweenPortalRotations, Vector3.up);
        portalRotationalDifference *= Quaternion.Euler(0, -90, 0);
        Vector3 newCameraDirection = portalRotationalDifference * playerCamera.forward;
        //newCameraDirection = Quaternion.Euler(0, 90, 0);
        transform.rotation = Quaternion.LookRotation(newCameraDirection, Vector3.up);
    }
}
